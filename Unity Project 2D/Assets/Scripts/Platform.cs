using System.Collections;
using UnityEngine;

public class Platform : MonoBehaviour
{
    [SerializeField] private bool isAnimated;
    [SerializeField] private float animationDistanse = 5f;
    private float startX;
    private void Start()
    {
        startX = transform.position.x;
        if (isAnimated)
        {
            StartCoroutine(AnimationProcess());
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        bool isMovedObject = other.GetComponent<CharacterMovement>();
        if (isMovedObject)
        {
            other.transform.parent = transform;
        }
    }

    private IEnumerator AnimationProcess()
    {

        var rightMovement = true;
        var delta = 0f;
        
        while (true)
        {
            delta += Time.deltaTime;
            if (delta>1f)
            {
                delta = 0;
                rightMovement = !rightMovement;
            }
            Vector3 position = transform.position;
            var from = rightMovement ?startX:startX+animationDistanse;
            var to = rightMovement ? startX + animationDistanse : startX;
            position.x = Mathf.Lerp(from, to, delta);
            transform.position = position;
            yield return null;

        }
    }
}