using System;
using UnityEngine;

public enum EEnemyState
{
    Sleep,
    Wait,
    StartWalk,
    Walk,
    StartAttack,
    Attack,
    Run,
}

public class BaseEnemy : MonoBehaviour, IEnemy, IHitBox
{
    //State mashine
    [SerializeField] private Animator animator;
    [SerializeField] private Transform checkGroundPoint;
    [SerializeField] private Transform checkAttackPoint;
    [SerializeField] private Transform graphics;

    private GameManager gameManager;

    private EEnemyState currentState = EEnemyState.Sleep;

    private float wakeUpTimer;
    private float waitTimer;
    private float attackTimer;
    private EEnemyState nextState;
    private float currentDirection = 1f;
    
    
    
    public void RegisterEnemy()
    {
        gameManager = FindObjectOfType<GameManager>();
        
        gameManager.Enemies.Add(this);
    }

    private void Awake()
    {
        RegisterEnemy();
        wakeUpTimer = Time.time + 1f;

    }
    
    [SerializeField] private int health = 5;


    public int Health
    {
        get => health;
        private set
        {
            health = value;
            if (health <= 0)
            {
                Die();
            }
        }
    }
    
    public void Hit(int damage)
    {
        Health -= damage;
        currentState = EEnemyState.Run;
    }

    public void Die()
    {
        animator.SetTrigger("Die");
        Destroy(this);
        Destroy(gameObject,1f);
    }


    private void Update()
    {
        switch (currentState)
        {
            case EEnemyState.Sleep:
                Sleep();
                break;
            
            case EEnemyState.Wait:
                Wait();
                break;
            
            case EEnemyState.StartWalk:
                animator.SetInteger("Walking",1);
                currentState = EEnemyState.Walk;
                break;
            
            case EEnemyState.Walk:
                Walk();
                break;
            
            case EEnemyState.StartAttack:
                animator.SetTrigger("Attack");
                ((IHitBox) gameManager.Player).Hit(1);
                currentState = EEnemyState.Attack;
                break;
            
            case EEnemyState.Attack:
                Attack();
                break;
            
            case EEnemyState.Run:
                Run();
                break;
            
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void StartSleeping(float sleepTime = 1f)
    {
        wakeUpTimer = Time.time + sleepTime;
        currentState = EEnemyState.Sleep;
    }

    private void Sleep()
    {
        if (Time.time>= wakeUpTimer)
        {
            WakeUp();
        }
    }
    
    private void WakeUp()
    {
        var playerPosition = ((MonoBehaviour) gameManager.Player).transform.position;
        
        
        if (Vector3.Distance(transform.position, playerPosition)>20f)
        {
            StartSleeping();
            return;
        }

        currentState = EEnemyState.Wait;
        nextState = EEnemyState.StartWalk;
        waitTimer = Time.time + 0.1f;
        
    }
    private void Wait()
    {
        if (Time.time>=waitTimer)
        {
            currentState = nextState;
        }
    }
    
    private void Walk()
    {
        transform.Translate(transform.right*Time.deltaTime*currentDirection);
        //Проверяем возможность передвежения - край платформы
        RaycastHit2D hit = Physics2D.Raycast(checkGroundPoint.position, Vector2.down, 0.3f);
        if (hit.collider == null)
        {
            currentDirection *= -1;
            float angle = currentDirection > 0 ? 0f:180f;
            graphics.localEulerAngles = new Vector3(0,angle,0f);
            currentState = EEnemyState.Wait;
            waitTimer = Time.time + 0.3f;
            
            animator.SetInteger("Walking",0);
            return;
        }
        
        hit = Physics2D.Raycast(checkAttackPoint.position, Vector2.right, 0.3f);
        if (hit.collider ==null)
        {
            return;
        }
        var player = hit.collider.GetComponent<Player>();
        if (player)
        {
            currentState = EEnemyState.StartAttack;
        }
    }
    
    void Attack()
    {
        if (Time.time < attackTimer)
        {
            return;
        }

        currentState = EEnemyState.Wait;
        nextState = EEnemyState.StartWalk;
        waitTimer = Time.time + 0.2f;
    }

    void Run()
    {
        print("run");
        var point = transform.TransformPoint(((MonoBehaviour) gameManager.Player).transform.position);
        print(point.x);
        if (point.x>0&&currentDirection<0)
        {
            currentDirection *= -1;

        }

        if (point.x<0&&currentDirection>0)
        {
            currentDirection *= -1;
        }
        float angle = currentDirection > 0 ? 0f:180f;
        graphics.localEulerAngles = new Vector3(0,angle,0f);
        currentState = EEnemyState.Wait;
        waitTimer = Time.time + 0.3f;
        animator.SetInteger("Walking",0);
        
    }
}