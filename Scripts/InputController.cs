﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public static float HorizontalAxis;
    public static event Action<float> JumpAction;
    public static event Action<string> FireAction;
    private float jumpTimer;
    private Coroutine waitForJumpCoroutine;
    
    // Start is called before the first frame update
    private void Start()
    {
        HorizontalAxis = 0f;
    }

    // Update is called once per frame
    private void Update()
    {
        HorizontalAxis = Input.GetAxis("Horizontal");

        if (Input.GetButtonDown("Jump"))
        {
            if (waitForJumpCoroutine == null)
            {
                waitForJumpCoroutine = StartCoroutine(WaitJump());
                return;
            }
            jumpTimer = Time.time;
        }

        if (Input.GetButtonDown("Fire1"))
        {
            FireAction?.Invoke("Fire1");
        }
        
        if (Input.GetButtonDown("Fire2"))
        {
            FireAction?.Invoke("Fire2");
        }
    }

    private void OnDestroy()
    {
        HorizontalAxis = 0f;
    }

    private IEnumerator WaitJump()
    {
        yield return new WaitForSeconds(0.2f);
        if (JumpAction != null)
        {
            float force = Time.time - jumpTimer < 0.2f ? 1.25f : 1f;//TODO  это должно быть в константах
            JumpAction.Invoke(force);
        }

        waitForJumpCoroutine = null;
    }
}
