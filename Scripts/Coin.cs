﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private float movementDistance = 5f;
    
    private Coroutine moveUpCorotine;
    
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (moveUpCorotine==null && other.GetComponent<Player>())
        {
            moveUpCorotine = StartCoroutine(MoveUp());
        }
        
    }

    private IEnumerator MoveUp()
    {
        animator.SetTrigger("Rotate");
        float distanse = 0f;
        while (distanse<movementDistance)
        {
            var shift = movementDistance * Time.deltaTime;
            distanse += shift;
            transform.Translate(Vector3.up*shift);
            yield return null;
        }
        Destroy(gameObject);
    }
}
