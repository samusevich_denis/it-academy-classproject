﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private GameObject[] platform;
    [SerializeField] private GameObject[] cloud;
    [SerializeField] private GameObject traps;
    
    
    
    [SerializeField] private Vector3 startPos;
    [SerializeField] private Vector3 startPosCloud;
    [SerializeField] private float deltaY;
    [SerializeField] private float playerDelta;
    [SerializeField] private float deltaYCloud; 
    [SerializeField] private Vector3 startPosTraps;
    [SerializeField] private float deltaYTraps;
    
    // Start is called before the first frame update
    void Start()
    {
        
        for (int i = 0; i < 5; i++)
        {
            createRandomPlatform();
            createRandomCloud();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (player.position.y > startPos.y-playerDelta)
        {
            createRandomPlatform();
        }
        if (player.position.y > startPosCloud.y-playerDelta)
        {
            createRandomCloud();
        }
        if (player.position.y > startPosTraps.y-playerDelta)
        {
            createRandomTraps();
        }
    }

    private void createRandomPlatform()
    {
        var current = Random.Range(0, platform.Length);
        GameObject obj = Instantiate(platform[current]);
        startPos.x = Random.Range(-8, 4);
        obj.transform.position = startPos;
        
        startPos.y += deltaY;
    }

    private void createRandomCloud()
    {
        var current = Random.Range(0, cloud.Length);
        GameObject obj = Instantiate(cloud[current]);
        obj.transform.position = startPosCloud;
        startPosCloud.x = Random.Range(-8, 8);
        startPosCloud.y += deltaYCloud;
    }

    private void createRandomTraps()
    {
        GameObject obj = Instantiate(traps);
        startPosTraps.x = Random.Range(-8, 8);
        obj.transform.position = startPosTraps;
        
        startPosTraps.y += deltaYTraps;
    }
}
