﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControlller : MonoBehaviour
{
    [SerializeField] private Transform target;
    //[SerializeField] private float leftBound;
    //[SerializeField] private float rightBound;

    [SerializeField] private Transform spriteBackground;

    private void Update()
    {
        var position = target.position;

        position.x = transform.position.x;
        position.z = transform.position.z;

        position.y = Mathf.Lerp(
            transform.position.y, target.position.y, Time.deltaTime * 5f);

        //position.x = Mathf.Clamp(position.x, leftBound, rightBound);

        transform.position = position;
        position.z = 0f;
        spriteBackground.position = position;
    }
}
