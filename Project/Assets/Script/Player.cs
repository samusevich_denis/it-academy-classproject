﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    private Rigidbody2D rig;
    private float maxSpeed = 4f;
    public bool startJump = false;
    [SerializeField] private Transform checkGroundPoint;
    [SerializeField] private Animator animator;
    
    // Start is called before the first frame update
    void Start()
    {
        rig = GetComponent<Rigidbody2D>();
    }

    [SerializeField] private int health = 3;

    public int Health
    {
        get => health;
        private set
        {
            health = value;
            if (health<=0)
            {
                Die();
            }
        }
    }
    
    // Update is called once per frame
    void Update()
    {
        animator.SetFloat("velocity", rig.velocity.y);
        print(startJump);
        if (!startJump)
        {
            return;
        }
        if (rig.velocity.y<=0)
        {
            RaycastHit2D hit = Physics2D.Raycast(checkGroundPoint.position, Vector2.down, 0.3f);
            if (hit.collider != null)
            {
                var objHit = hit.transform.GetComponent<PlatUp>();
                if (objHit!=null)
                {
                    JumpUpUp();
                }
                Jump();
            }
        }

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other!=null)
        {
            
            if (other.GetComponent<Traps>())
            {
                SetDamage();
            }
        }
    }

    public void Jump()
    {
        rig.AddForce(new Vector2(0, 20), ForceMode2D.Impulse);
    }

    private void Move(Vector3 direction)
    {
        Vector2 velocity = rig.velocity;
        velocity.x = direction.x * maxSpeed;
        rig.velocity = velocity;
    }

    private void FixedUpdate()
    {
        var direction = new Vector2(InputController.HorizontalAxis, 0f);

        Move(direction);
    }
    
    public void Die()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void SetDamage()
    {
        print(Health);
        Health -= 1;
    }

    private void JumpUpUp()
    {
        rig.AddForce(new Vector2(0, 30), ForceMode2D.Impulse);
    }
}
