﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    [SerializeField] private Player player;
    public static float HorizontalAxis;
    
    // Start is called before the first frame update
    void Start()
    {
        HorizontalAxis = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        HorizontalAxis = Input.GetAxis("Horizontal");

        if (player.startJump)
        {
            return;
        }
        if (Input.GetButtonDown("Fire1"))
        {
            print("111"+player.startJump);
            player.startJump=true;
            player.Jump();
        }
        
    }
    
    
}
