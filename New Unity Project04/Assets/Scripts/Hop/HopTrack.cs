﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HopTrack : MonoBehaviour
{
    [SerializeField] private GameObject m_Platform;
    [SerializeField] private bool m_useRandomSeed;
    [SerializeField] private int m_seed = 123456;
    private List<GameObject> platforms = new List<GameObject>();
    
    
    // Start is called before the first frame update
    private void Start()
    {
        platforms.Add(m_Platform);

        if (m_useRandomSeed)
        {
            Random.InitState(m_seed);
        }
        
        for (int i = 0; i < 25; i++)
        {
            GameObject obj = Instantiate(m_Platform, transform);
            Vector3 pos = Vector3.zero;

            pos.z = 2 * (i + 1);
            int randomPos = Random.Range(-3, 4);
            pos.x = Random.Range(-3, 4);
            obj.transform.position = pos;

            obj.name = $"Platform_{i + 1}";
            platforms.Add(obj);
            if (Random.Range(0,2)==1)
            {   
                Vector3 newPos = Vector3.zero;
                int newRandomPos = Random.Range(-3, 4);
                while (randomPos==newRandomPos)
                {
                    newRandomPos = Random.Range(-3, 4);
                }
                GameObject newObj = Instantiate(m_Platform, transform);
                newPos.z = pos.z;
                newPos.x = newRandomPos;
                newObj.transform.position = newPos;

                newObj.name = $"Platform_{i + 1}.1";
                platforms.Add(newObj);
            }
        }
    }

    public bool IsBallOnPlatform(Vector3 position)
    {
        position.y = 0f;

        var nearestPlatform = platforms[0];

        for (int i = 1; i < platforms.Count; i++)
        {
            var platformZ = platforms[i].transform.position.z;
            var platformX = platforms[i].transform.position.x;
            if (platformZ + 0.5f < position.z)
            {
                continue;
            }
            if (platformZ - position.z > 0.5f)
            {
                continue;
            }
            nearestPlatform = platforms[i];
            if (platformX+0.5f> position.x&& platformX-0.5f<position.x)
            {
                nearestPlatform = platforms[i];
                break;
            }
        }

        float minX = nearestPlatform.transform.position.x - 0.5f;
        float maxX = nearestPlatform.transform.position.x + 0.5f;

        var platform = nearestPlatform.GetComponent<HotPlatform>();
        platform.SetGreen();
        return position.x > minX && position.x < maxX;

    }
}
