﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HotPlatform : MonoBehaviour
{
    [SerializeField] private GameObject m_baseView;

    [SerializeField] private GameObject m_greenVeiw;

    public void SetGreen()
    {
        m_baseView.SetActive(false);
        m_greenVeiw.SetActive(true);
        
        Invoke(nameof(SetBase),0.5f);
    }

    private void SetBase()
    {
        m_baseView.SetActive(true);
        m_greenVeiw.SetActive(false);
    }
    // Start is called before the first frame update
    void Start()
    {
        SetBase();
    }

}
