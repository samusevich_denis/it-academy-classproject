﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.SceneManagement;
using UnityEngine;

public class BallGame_Line : MonoBehaviour
{
    [SerializeField] private GameObject m_Gate;
    private List<BallGame_Gate> Gates = new List<BallGame_Gate>(); 
    private static int gateNumber = 0;
    
    public void CreateGate()
    {
        var gate = new GameObject($"gate {gateNumber}");
        gate.transform.position= new Vector3(0f,0f,-50f);
        var point = gate.transform.position;
        point.y = -5f;
        var obj = Instantiate(m_Gate);
        obj.transform.position = point;
        obj.transform.parent = gate.transform;
        var gateObj = gate.AddComponent<BallGame_Gate>();
        Gates.Add(gateObj);
        gateNumber++;

    }
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 6; i++)
        {
            CreateGate();
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (Gates[0].transform.position.z<-10f)
        {
            Gates.RemoveAt(0);
            CreateGate();
        }
    }
}
