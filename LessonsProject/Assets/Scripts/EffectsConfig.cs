﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EffectsConfig")]
public class EffectsConfig : ScriptableObject
{
    public GameObject[] effects;
}
