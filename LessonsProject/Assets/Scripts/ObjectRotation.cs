﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotation : MonoBehaviour
{
    [SerializeField] private Transform targetPoint;

    [SerializeField] private float spead = 45f;


    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(targetPoint.position, targetPoint.up,spead*Time.deltaTime);
    }
}
