﻿using System.Collections;
using System.Collections.Generic;
using Tutorial;
using UnityEngine;

namespace Tutorial
{
    public class TutorialStepUiAbstract : MonoBehaviour, ITutorialStep
    {
        [SerializeField] private int id;
  
        public int Id => id;
        public virtual bool InitStep()
        {
            return TutorialController.Instance.Add(this);
        }

        public virtual void StartStep()
        {
            
        }

        public virtual void StopStep()
        {
            TutorialController.Instance.Stop(id);
        }

        protected void Awake()
        {
            if (!InitStep())
            {
                Destroy(this);
            }
        }
    }


}
