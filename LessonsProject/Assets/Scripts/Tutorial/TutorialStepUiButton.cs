﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Tutorial
{
    public class TutorialStepUiButton : TutorialStepUiAbstract
    {
        [SerializeField] private Button button;

        public override bool InitStep()
        {
            button.gameObject.SetActive(true);
            var isAdded = base.InitStep();
            if (isAdded)
            {
                return true;
            }
            button.gameObject.SetActive(true);
            return false;
        }


        public override void StartStep()
        {
            base.StartStep();
            button.gameObject.SetActive(true);
            button.onClick.AddListener(StopStep);
        }

        public override void StopStep()
        {
            base.StopStep();
            Destroy(this);
        }
        
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }

}

