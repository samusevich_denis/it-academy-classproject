﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMesh : MonoBehaviour
{
    private MeshFilter _meshFilter;
    private Vector3[] vertices;
    private float posMinY;
    private float posMaxY;
    List<List<int>> indexVertices = new List<List<int>>();
    [SerializeField]private AnimationCurve animationCurve;

    private float time;
    // Start is called before the first frame update
    void Start()
    {
        var meshFilter = GetComponent<MeshFilter>();
        vertices = meshFilter.sharedMesh.vertices;

        

        bool flagNewVertices = true;
        for (int i = 0; i < vertices.Length; i++)
        {
            flagNewVertices = true;
            for (int j = 0; j < indexVertices.Count; j++)
            {

                if (EqualsVector3(vertices[i], vertices[indexVertices[j][0]] ))
                {
                    flagNewVertices = false;
                    indexVertices[j].Add(i);
                }
            }
            if (flagNewVertices)
            {
                indexVertices.Add(new List<int>());
                indexVertices[indexVertices.Count - 1].Add(i);
            }
        }
        posMinY = vertices[indexVertices[0][0]].y;
        for (int i = 0; i < indexVertices.Count; i++)
        {
            if (vertices[indexVertices[i][0]].y < posMinY)
            {
                posMinY = vertices[indexVertices[i][0]].y;
            }
        }
        posMaxY = vertices[indexVertices[0][0]].y;
        for (int i = 0; i < indexVertices.Count; i++)
        {
            if (vertices[indexVertices[i][0]].y > posMaxY)
            {
                posMaxY = vertices[indexVertices[i][0]].y;
            }
        }
    }

    private static bool EqualsVector3(Vector3 vectorOne, Vector3 vectorTwo)
    {
        if (Mathf.Abs(vectorOne.x - vectorTwo.x) > 0.0001)
        {
            return false;
        }
        if (Mathf.Abs(vectorOne.y - vectorTwo.y) > 0.0001)
        {
            return false;
        }
        if (Mathf.Abs(vectorOne.z - vectorTwo.z) > 0.0001)
        {
            return false;
        }
        return true;
    }
    
    
    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < indexVertices.Count; i++)
        {
            for (int j = 0; j < indexVertices[i].Count; j++)
            {
                var cur = vertices[indexVertices[i][j]].y / posMaxY + time / 10;
                if (cur>1)
                {
                    cur -= 1;
                }
                float pos = animationCurve.Evaluate(cur);
                vertices[indexVertices[i][j]].x += pos;
            }
            Debug.Log(indexVertices[i].ToString());
        }
        _meshFilter.sharedMesh.vertices = vertices;
        time += Time.deltaTime;
        if (time>10)
        {
            time -= 10;
        }
    }
    
}
