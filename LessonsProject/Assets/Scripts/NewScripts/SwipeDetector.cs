﻿using System;
using UnityEngine;

public class SwipeDetector : MonoBehaviour
{
    public event Action<int> OnSwipe;


    private float startX;
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            startX = Input.mousePosition.x;
        }

        if (Input.GetMouseButtonUp(0))
        {
            var x = Input.mousePosition.x;

            if (Math.Abs(x - startX) > 100)
            {
                OnSwipe?.Invoke(100);
            }
        }
    }
}
