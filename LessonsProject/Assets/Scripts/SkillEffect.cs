﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillEffect : MonoBehaviour
{
    [SerializeField] private int damage = 5;

    [SerializeField] private float speed = 2f;

    [SerializeField] private Transform mainSphere;

    [SerializeField] private AnimationCurve mainPeriodAnimationCurve;

    [SerializeField] private float mainPeriodTime = 2f;

    private float period;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(speed*Time.deltaTime*transform.forward);
        if (period>1f)
        {
            period = 0f;
        }

        var scale = mainPeriodAnimationCurve.Evaluate(period);
        mainSphere.localPosition = Vector3.one * scale;
        period += Time.deltaTime / mainPeriodTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        var health = other.transform.GetComponent<Health>();
        if (health!=null)
        {
            health.SetDamage(damage);
        }
    }
}
