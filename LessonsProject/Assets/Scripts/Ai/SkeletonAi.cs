using System;
using UnityEngine;

namespace Ai
{
    public class SkeletonAi : MonoBehaviour
    {
        [SerializeField] private Node rootNode;

        private void Update()
        {
            rootNode.Evaluate();
        }
    }
}