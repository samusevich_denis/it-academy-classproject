﻿using System.Collections;
using System.Collections.Generic;
using ECS;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using UnityEngine;

public class EffectMovementSystem : JobComponentSystem
{
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        var deltaTime = World.Time.DeltaTime;

        var job = Entities.ForEach((ref Translation translation, in EffectMovementComponent component) =>
        {
            translation.Value += component.Direction * component.Speed * deltaTime;
            
        }).Schedule(inputDeps);
        
        job.Complete();
        return job;
    }
}
