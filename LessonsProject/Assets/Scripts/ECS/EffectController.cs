﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using Unity.Entities;
using Unity.Mathematics;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace ECS
{
    public class EffectController : MonoBehaviour
    {
        public static EntityManager EntityManager;

        [SerializeField] private GameObject[] effectPrefabs;
        [SerializeField] private float minSpeed = 1.5f;
        [SerializeField] private float maxSpeed = 2.5f;
        [SerializeField] private uint effectsIterations = 40;
        [SerializeField] private float effectStep =0.1f;
        [SerializeField] private float effectsWidth = 0.5f;
        [SerializeField] private int objectsInLine = 3;

        private GameObject[] effectObjects;
        private Coroutine effectCorutine;

        private void Start()
        {
            EntityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            effectObjects = new GameObject[effectPrefabs.Length];
            for (int i = 0; i < effectObjects.Length; i++)
            {
                effectObjects[i] = Instantiate(effectPrefabs[i]);
                effectObjects[i].SetActive(false);
            }
        }
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (effectCorutine!=null)
                {
                    return;
                }

                effectCorutine = StartCoroutine(EffectProcess(new Vector3(0f, 1.2f, -6f)));
            }
        }

        private IEnumerator EffectProcess(Vector3 startPoint)
        {
            var counter = 0;

            while (counter<effectsIterations)
            {
                foreach (var effectObject in effectObjects)
                {
                    effectObject.SetActive(true);
                }

                for (int i = 0; i < objectsInLine; i++)
                {
                    var x = UnityEngine.Random.Range(-effectsWidth, effectsWidth) + startPoint.x;
                    var z = counter * effectStep + startPoint.z;
                    var position = new Vector3(x,0.2f,z);
                    var direction = new float3(0f, 1f, 0f);
                    var speed = UnityEngine.Random.Range(minSpeed, maxSpeed);
                    var obj = effectObjects[UnityEngine.Random.Range(0, effectObjects.Length)];
                    obj.transform.position = transform.position;
                    obj.transform.rotation = quaternion.identity;

                    SetupObject(obj, direction,speed);
                }
                counter++;
                
                foreach (var effectObject in effectObjects)
                {
                    effectObject.SetActive(false);
                }
                yield return null;
            }

            effectCorutine = null;
        }

        private void SetupObject(GameObject effectObj, float3 dir, float speed)
        {
            var conversionSetting = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, null);

            var effectEntity = GameObjectConversionUtility.ConvertGameObjectHierarchy(effectObj, conversionSetting);

            var movementData = new EffectMovementComponent
            {
                Direction = dir,
                Speed = speed
            };

            EntityManager.AddComponent<EffectMovementComponent>(effectEntity);
            EntityManager.SetComponentData(effectEntity,movementData);
        }
        
    }
    
}
