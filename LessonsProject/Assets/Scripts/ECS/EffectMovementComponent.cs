﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace ECS
{
    [GenerateAuthoringComponent]
    public struct EffectMovementComponent : IComponentData
    {
        public float3 Direction;
        public float Speed;
    }

}

