﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
#if UNITY_EDITOR
public class CreateAssetsBundles 
{
    [MenuItem("Assets/Build Assets bundles")]
    private static void BuildAllAssetsBundels()
    {
        BuildPipeline.BuildAssetBundles("assets/AssetBundles",BuildAssetBundleOptions.None,BuildTarget.StandaloneWindows);
        AssetDatabase.Refresh();
    }
    
    [MenuItem("Assets/Get Assets bundles names")]
    private static void GetNames()
    {
        var names = AssetDatabase.GetAllAssetBundleNames();
        foreach (var name in names)
        {
            Debug.Log(name);
        }
    }
}


#endif

