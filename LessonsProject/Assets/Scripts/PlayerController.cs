﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public enum CharacterState
{
    Idle,
    Move,
    Attack,
    Skill,
    Hit,
    Dead,
}

[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour
{
    private CharacterState characterState;
    [Header("Main")]
    [SerializeField] private Animator animator;
    [SerializeField] private GameObject ballPrefab;
    [SerializeField] private Transform firePoint;
    [SerializeField] private int skillBallsCount;
    [SerializeField] private float skillAngleBound = 45f;
    [SerializeField] private Transform deadShotPoint;
 
    [Header("Bonuses")] 
    [SerializeField] private GameObject hat;

    [SerializeField] private ParticleSystem auraEffect;
    [SerializeField] private GameObject skillEffect;
    
    private static readonly int AttackTrigger = Animator.StringToHash("Attack");
    private static readonly int SkillTrigger = Animator.StringToHash("Skill");
    private static readonly int Movement = Animator.StringToHash("Movement");

    public void AttackEvent()
    {
        var obj = ObjectsPool.Instance.GetObject(ballPrefab, firePoint.transform.position, Quaternion.identity);


        var rig = obj.GetComponent<Rigidbody>();
        if (rig)
        {
            rig.velocity = Vector3.zero;
            rig.AddForce(Vector3.forward * 5f, ForceMode.Impulse);
        }
    }

    public void WearHat()
    {
        hat.SetActive(true);
    }
    
    public void SkillEvent()
    {
        var skillObj = ObjectsPool.Instance.GetObject(skillEffect);
        skillObj.transform.position = firePoint.position;
        skillObj.transform.rotation = quaternion.identity;
        return;
        var angleStep = skillAngleBound * 2 / (skillBallsCount - 1);
        
        for (var i = 0; i < skillBallsCount; i++)
        {
            var y = skillAngleBound - i * angleStep;
            var rotation = Quaternion.Euler(0f, y, 0f);
            
            var obj = ObjectsPool.Instance.GetObject(ballPrefab, firePoint.transform.position, rotation);
            
            obj.transform.Translate(obj.transform.forward * 0.3f);
            
            var rig = obj.GetComponent<Rigidbody>();
            if (rig)
            {
                rig.velocity = Vector3.zero;
                rig.AddForce(obj.transform.forward * 5f, ForceMode.Impulse);
            }
        }
    }
    
    private void Reset()
    {
        animator = GetComponent<Animator>();
    }

    // Start is called before the first frame update
    private void Start()
    {
        characterState = CharacterState.Move;
        InputController.OnInputAction += OnInputCommand;

        StartCoroutine(MovementProcess(3f));
        
        hat.SetActive(false);
        
        ObjectsPool.Instance.AddObjects(ballPrefab,10);
    }

    private IEnumerator MovementProcess(float time)
    {
        animator.SetInteger(Movement, 1);
        var timer = Time.time + time;
        while (Time.time < timer)
        {
            transform.Translate(transform.forward * Time.deltaTime);
            yield return null;
        }

        characterState = CharacterState.Idle;
        animator.SetInteger(Movement, 0);
    }

    private void OnDestroy()
    {
        InputController.OnInputAction -= OnInputCommand;
    }

    private void OnInputCommand(InputCommand inputCommand)
    {
        switch (inputCommand)
        {
            case InputCommand.Fire:
                Attack();
                break;
            case InputCommand.Skill:
                Skill();
                break;
            case  InputCommand.DeadShot:
                DeadShot();
                break;
            case  InputCommand.SuperShot:
                SuperShot();
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(inputCommand), inputCommand, null);
        }
    }

    private void Attack()
    {
        if (characterState == CharacterState.Attack || characterState == CharacterState.Skill)
        {
            return;
        }

        animator.SetTrigger(AttackTrigger);
        characterState = CharacterState.Attack;
        DelayRun.Execute(delegate
        {
            characterState = CharacterState.Idle;
        }, 0.5f, gameObject);
    }

    private void Skill()
    {
        if (characterState == CharacterState.Attack || characterState == CharacterState.Skill)
        {
            return;
        }
        
        auraEffect.Play();
        
        animator.SetTrigger(SkillTrigger);
        characterState = CharacterState.Skill;
        DelayRun.Execute(delegate
        {
            characterState = CharacterState.Idle;
        }, 1f, gameObject);
    }

    private void DeadShot()
    {
        if (characterState == CharacterState.Attack || characterState == CharacterState.Skill)
        {
            return;
        }
        animator.SetTrigger(SkillTrigger);
        characterState = CharacterState.Skill;

        DelayRun.Execute(delegate { characterState = CharacterState.Idle; }, 1.65f, gameObject);

        DelayRun.Execute(delegate
        {
            var hit = new RaycastHit();
            if (!Physics.Raycast(deadShotPoint.position, deadShotPoint.forward, out hit))
            {
                return;
            }

            var health = hit.transform.GetComponent<Health>();
            if (health != null)
            {
                health.SetDamage(int.MaxValue);
            }
        }, 1.35f, gameObject);

    }

    private void SuperShot()
    {
        if (characterState == CharacterState.Attack || characterState == CharacterState.Skill)
        {
            return;
        }

        animator.SetTrigger(SkillTrigger);
        characterState = CharacterState.Skill;

        DelayRun.Execute(delegate { characterState = CharacterState.Idle; }, 1.65f, gameObject);

        DelayRun.Execute(delegate
        {
            var hitRay = new RaycastHit();
            if (!Physics.Raycast(deadShotPoint.position, deadShotPoint.forward, out hitRay))
            {
                return;
            }

            var hits = Physics.RaycastAll(deadShotPoint.position, deadShotPoint.forward);
            foreach (var hit in hits)
            {
                var health = hit.transform.GetComponent<Health>();
                if (health != null)
                {
                    health.SetDamage(int.MaxValue);
                }
            }
        }, 1.35f, gameObject);
        
        

    }
}

