﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Profile
{
    
    [Serializable]
    public class MainData
    {
        public List<int> levelStars = new List<int>();
        public int money = 10;
    }
    
        
    [Serializable]
    public class PlayerData
    {
        public bool sound = true;
        public bool music = true;
    }

    private static MainData mainData;
    private static PlayerData playerData;

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void CheckData()
    {
        CheckMainData();
        CheckPlayerData();
    }

    private static void CheckMainData()
    {
        Debug.Log("Init Profile Main Data");
        if (mainData!=null)
        {
            return;
        }
        
        if (PlayerPrefs.HasKey("MainDate"))
        {
            mainData = new MainData();
            PlayerPrefs.SetString("MainData",JsonUtility.ToJson(mainData));
        }

        mainData = GetData<MainData>("MainDate");
    }

    private static void CheckPlayerData()
    {
        Debug.Log("Init Profile Main Data");
        if (playerData!=null)
        {
            return;
        }
        
        if (PlayerPrefs.HasKey("PlayerData"))
        {
            playerData = new PlayerData();
            PlayerPrefs.SetString("PlayerData",JsonUtility.ToJson(playerData));
        }

        playerData = GetData<PlayerData>("PlayerData");
    }

    private static T GetData<T>(string key) where T : new()
    {
        if (PlayerPrefs.HasKey(key))
        {
            return JsonUtility.FromJson<T>(PlayerPrefs.GetString(key));
        }
        var data = new T();
        PlayerPrefs.SetString(key, JsonUtility.ToJson(data));
        return data;
    }

    public static void Save(bool main = true, bool player = true)
    {
        if (main)
        {
            PlayerPrefs.SetString("MainDate",JsonUtility.ToJson(mainData));
        }
        if (player)
        {
            PlayerPrefs.SetString("PlayerData",JsonUtility.ToJson(playerData));
        }
    }

    public static int Money
    {
        get => mainData.money;
        set
        {
            mainData.money = value;
            if (mainData.money<0)
            {
                mainData.money = 0;
            }
            Save(player:false);
        }
    }

    public static int OpenedLevelCount => mainData.levelStars.Count;

    public static int GetLevelStars(int level)
    {
        if (level >= mainData.levelStars.Count)
        {
            return -1;
        }

        return mainData.levelStars[level];
    }

    public static void SetLevelStars(int level, int stars)
    {
        if (level>mainData.levelStars.Count)
        {
            Debug.Log($"Level {level} was not open");
        }

        if (level == mainData.levelStars.Count)
        {
            stars = Mathf.Clamp(stars, 0, 3);
            mainData.levelStars.Add(stars);
            Save(player:false);
            return;
        }

        if (stars> mainData.levelStars[level])
        {
            stars = Mathf.Clamp(stars, 0, 3);
            mainData.levelStars[level] = stars;
            Save(player:false);
        }
    }
}
