﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameMenu : MonoBehaviour
{
    [SerializeField] private GameObject mainPanel;
    [SerializeField] private GameObject pausePanel;
    [SerializeField] private Button pauseButton;
    [SerializeField] private Button exitButton;
    [SerializeField] private Button resumeButton;

    private void Start()
    {
        mainPanel.SetActive(true);
        pausePanel.SetActive(false);
       
        
        pauseButton.onClick.AddListener(OnPauseButton);
        exitButton.onClick.AddListener(OnExitButton);
        resumeButton.onClick.AddListener(OnResumeButton);
    }

    private void OnPauseButton()
    {
        Time.timeScale = 0f;
        mainPanel.SetActive(false);
        pausePanel.SetActive(true);
    }
    
    private void OnExitButton()
    {
        Time.timeScale = 1f;
        mainPanel.SetActive(true);
        pausePanel.SetActive(false);
    }
        
    private void OnResumeButton()
    {
        //SceneManager;
    }
}
