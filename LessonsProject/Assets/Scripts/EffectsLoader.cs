﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class EffectsLoader : MonoBehaviour
{
    private void Start()
    {
        StartCoroutine(LoadBundle());
    }

    private AssetBundle bundle;
#if UNITY_EDITOR
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            var obj = LoadEffects("Effect1");
            Destroy(obj, 2f);
#if DEBUGER
            Debug.Log(0);
#endif
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            var obj = LoadEffects("Effect2");
            Destroy(obj, 2f);
#if DEBUGER
            Debug.Log(1);
#endif
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            var obj = LoadEffects("Effect3");
            Destroy(obj, 2f);
#if DEBUGER
            Debug.Log(2);
#endif
        }

    }
#endif

    private IEnumerator LoadBundle()
    {
        var path = "https://drive.google.com/uc?export=download&id=1IC1nWbEYsk5-jl203-OGwl1isP4EFAjk";
        using (UnityWebRequest webRequest = UnityWebRequestAssetBundle.GetAssetBundle(path))
        {
            yield return webRequest.SendWebRequest();
            if (webRequest.isNetworkError|| webRequest.isNetworkError)
            {
                Debug.LogError(webRequest.error);
            }
            else
            {
                bundle = DownloadHandlerAssetBundle.GetContent(webRequest);
                Debug.Log($"Bundle {bundle.name} loader");
            }
        }
    }
    public GameObject LoadEffects(string effectName)
    {
        if (!bundle)
        {
            var path = Path.Combine(Application.dataPath, "AssetsBundles/effects");
            bundle = AssetBundle.LoadFromFile(path);
            Debug.Log("Load from disk");
        }

        if (bundle==null)
        {
            Debug.LogError("Bundle loading error");
        }

        var prefab = bundle.LoadAsset<GameObject>(effectName);
        return prefab ? Instantiate(prefab) : null;
        return null;
        //var path = $"Effects/EffectsConfig";
        //var congig = Resources.Load<EffectsConfig>(path);
        //var prefab = congig.effects[nm];
        //return prefab ? Instantiate(prefab) : null;
    }
}
