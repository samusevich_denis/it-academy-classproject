﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformsGenerator : MonoBehaviour
{

    [SerializeField] private GameObject platformPrefabs;

    [SerializeField] private Transform target;

    [SerializeField] private float distance;
    [SerializeField] private float min = 10f;
    [SerializeField] private float max = 10f;

    private List<Transform> platforms = new List<Transform>();
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        UpdatePlatform();
    }

    private void SetupPlatforms()
    {
        var minY = target.position.y - min;
        var maxY = target.position.y + max;
        
        
        
        var pos = new Vector3(0f,minY,0f);
        while (pos.y<maxY)
        {
            var obj = ObjectsPool.Instance.GetObject(platformPrefabs);
            obj.transform.position = pos;

            platforms.Add(obj.transform);

            pos.y += distance;
        }
    }
    private void UpdatePlatform()
    {
        var minY = target.position.y - min;
        var maxY = target.position.y + max;

        foreach (var platform in platforms)
        {
            if (platform.position.y <maxY)
            {
                platforms.Remove(platform);
                platform.gameObject.SetActive(false);
                break;
            }
        }

        var pos = platforms[platforms.Count - 1].position;
        pos.y += distance;
        while (pos.y<maxY)
        {
            AddPlatform(pos);
            pos.y += distance;
        }
    }

    private void AddPlatform(Vector3 pos)
    {
        var obj = ObjectsPool.Instance.GetObject(platformPrefabs);
        obj.transform.position = pos;
        
        platforms.Add(obj.transform);
    }
}
