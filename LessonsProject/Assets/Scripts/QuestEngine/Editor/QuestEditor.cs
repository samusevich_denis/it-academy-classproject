﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace QuestEngine.Data
{
    public class QuestEditor : EditorWindow
    {
        public QuestData data;


        private static void ShowWindow()
        {
            var window = GetWindow<QuestEditor>();
            //window.titleContent
        }

        private void Load()
        {
            var filePath = EditorUtility.OpenFilePanel("Select data file", Application.dataPath, "json");
            if (string.IsNullOrEmpty(filePath)) return;
            var dataAsJson = File.ReadAllText(filePath);
            data = JsonUtility.FromJson<QuestData>(dataAsJson);
        }
    }

}

