﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuestEngine.Data
{
    
    public interface IQuestState 
    {
        string Id { get; }
        //string[]
        void Start();
        void Check();
        void Complete();
        void Fail();
    }
}

