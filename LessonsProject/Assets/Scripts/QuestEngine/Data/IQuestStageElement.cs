﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuestEngine.Data
{
    
    public interface IQuestStateElement 
    {
        string Id { get; }
        
    }
}

