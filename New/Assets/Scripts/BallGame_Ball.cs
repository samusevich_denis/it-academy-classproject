﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BallGame_Ball : MonoBehaviour
{
    //[SerializeField] private AnimationCurve m_JumpCurve;
    //[SerializeField] private float m_JumpHeight = 1f;
    //[SerializeField] private float m_JumpDistance = 2f;
    //[SerializeField] private float m_BallSpeed = 1f;
    [SerializeField] private BallGame_Input m_Input;
    private Vector3 position;
    //[SerializeField] private BallGame_Line m_Line;

    // Start is called before the first frame update
    


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var pos = transform.position;
        pos.x = Mathf.Lerp(pos.x, m_Input.Strafe, Time.deltaTime * 5f);
        transform.position = pos;
    }
}
