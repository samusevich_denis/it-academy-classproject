﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BallGame_Line : MonoBehaviour
{
    [SerializeField] private GameObject m_Gate;
    private List<BallGame_Gate> Gates = new List<BallGame_Gate>(); 
    private static int gateNumber = 0;
    [SerializeField] private BallGame_Ball m_Ball;
    
    public void CreateGate(Vector3 pos)
    {
        var gate = new GameObject($"gate {gateNumber}");
        gate.transform.position = pos;
        var point = gate.transform.position;
        point.y = -4f;
        var obj = Instantiate(m_Gate);
        obj.transform.position = point;
        obj.transform.parent = gate.transform;
        var gateObj = gate.AddComponent<BallGame_Gate>();
        Gates.Add(gateObj);
        gateNumber++;

    }
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 7; i++)
        {
            var pos = new Vector3(0f,0f,10f*i+20f);
            CreateGate(pos);
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (Gates[Gates.Count-1].transform.position.z<60f)
        {
            CreateGate(new Vector3(0f,0f,70f));
        }

        if (Gates[0].transform.position.z <-1)
        {
            var pos = m_Ball.transform.position.x - Gates[0].transform.position.x;
            if (Mathf.Abs(pos)<0.3f)
                
            {
                return;
            }
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        if (Gates[0].transform.position.z<-3f)
        {
            Destroy(Gates[0].gameObject);
            Gates.RemoveAt(0);
        }
    }
}
