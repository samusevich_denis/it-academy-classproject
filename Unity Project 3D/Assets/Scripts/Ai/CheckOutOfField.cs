namespace Ai
{
    public class CheckOutOfField : Node
    {
        public override NodeState Evaluate()
        {
            return transform.position.z > 8f ? NodeState.Running : NodeState.Failure;
        }
    }
}