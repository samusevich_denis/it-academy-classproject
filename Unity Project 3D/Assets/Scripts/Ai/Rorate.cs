using UnityEngine;

namespace Ai
{
    public class Rorate : Node
    {
        [SerializeField] private Transform root;
        
        public override NodeState Evaluate()
        {
            transform.Rotate(0f,180f,0f);
            return NodeState.Success;
        }
    }
}