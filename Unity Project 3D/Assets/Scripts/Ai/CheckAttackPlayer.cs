using System;

namespace Ai
{
    public class CheckAttackPlayer : Node
    {
        public bool isCanRotate;
        
        public override NodeState Evaluate()
        {
            return isCanRotate ? NodeState.Running : NodeState.Failure;
        }
    }
}