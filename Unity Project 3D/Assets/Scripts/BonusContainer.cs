﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusContainer : MonoBehaviour
{
    [SerializeField] private Health health;
    [SerializeField] private GameObject bonus;


    private void Reset()
    {
        health = GetComponent<Health>();
    }


    // Start is called before the first frame update
    void Start()
    {
        if (health)
        {
            health.DieAction += OnDie;
            return;

        }
        Destroy(this);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDie()
    {
        if (bonus)
        {
            Instantiate(bonus, transform.position, transform.rotation);
        }
    }
}
