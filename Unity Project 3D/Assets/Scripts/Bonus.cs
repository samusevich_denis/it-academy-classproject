﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(SphereCollider))]
public class Bonus : MonoBehaviour
{
    
    // Start is called before the first frame update
    protected void Reset()
    {
        var rig = GetComponent<Rigidbody>();
        rig.isKinematic = true;

        var sphereCollider = GetComponent<SphereCollider>();
        sphereCollider.radius = 0.56f;
        sphereCollider.isTrigger = true;
    }

    // Update is called once per frame
    protected void Update()
    {
        transform.Translate(Time.deltaTime*2f*Vector3.back, Space.World);
    }

    protected void OnMouseDown()
    {
        PickUpBonus();
    }

    protected virtual void PickUpBonus()
    {
        StartCoroutine(MoveUp());
        var sphereCollider = GetComponent<SphereCollider>();
        if (sphereCollider)
        {
            sphereCollider.enabled = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponent<PlayerController>();
        if (player)
        {
            StartCoroutine(MoveUp());
        }
    }

    protected IEnumerator MoveUp()
    {
        var height = 0f;
        while (height<10f)
        {
            height += Time.deltaTime * 10;
            var pos = transform.position;
            transform.Rotate(0f,5,0f);
            pos.y = height;
            transform.position = pos;
            yield return null;
        }
    }
}
